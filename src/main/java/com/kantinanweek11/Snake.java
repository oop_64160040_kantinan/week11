package com.kantinanweek11;

public class Snake extends Animal implements Swimable ,Crawlable{

    public Snake(String name) {
        super(name, 0);
    }

    @Override
    public void crawl() {
        System.out.println(this.toString() + "crawl.");
        
    }

    @Override
    public void swim() {
        System.out.println(this.toString() + "swim.");
        
    }

    @Override
    public void eat() {
        System.out.println(this.toString() + "eat.");
        
    }

    @Override
    public void sleep() {
        System.out.println(this.toString() + "sleep.");
        
    }
    @Override
    public String toString() {
        return "Snake" + "(" + this.getName() + ")";
    }
    
}
