package com.kantinanweek11;

public interface Walkable {
    public void walk();
    public void run();
}
