package com.kantinanweek11;

public class App {
    public static void main(String[] args) {
        Bat bat1 = new Bat("Batcat");
        bat1.eat();
        bat1.sleep();
        bat1.fly();
        bat1.takeoff();
        bat1.landing();
        System.out.println("-------------------------------------");
        Bird bird1 = new Bird("Noks");
        bird1.eat();
        bird1.sleep();
        bird1.fly();
        bird1.takeoff();
        bird1.landing();
        bird1.walk();
        bird1.run();
        System.out.println("-------------------------------------");
        Cat cat1 = new Cat("Mewwww");
        cat1.eat();
        cat1.sleep();
        cat1.walk();
        cat1.run();
        System.out.println("-------------------------------------");
        Crocodile crocodile1 = new Crocodile("Mr.0");
        crocodile1.eat();
        crocodile1.sleep();
        crocodile1.crawl();
        System.out.println("-------------------------------------");
        Dog dog1 = new Dog("Doedik");
        dog1.eat();
        dog1.sleep();
        dog1.walk();
        dog1.run();
        dog1.swim();
        System.out.println("-------------------------------------");
        Fish fish1 = new Fish("Panamkam");
        fish1.eat();
        fish1.sleep();
        fish1.swim();
        System.out.println("-------------------------------------");
        Human human1 = new Human("Nine");
        human1.eat();
        human1.sleep();
        human1.walk();
        human1.run();
        human1.swim();
        System.out.println("-------------------------------------");
        Plane plane1 = new Plane("Boing", "BoingEngin");
        plane1.fly();
        plane1.takeoff();
        plane1.landing();
        System.out.println("-------------------------------------");
        Rat rat1 = new Rat("Tane");
        rat1.eat();
        rat1.sleep();
        rat1.walk();
        rat1.run();
        System.out.println("-------------------------------------");
        Snake snake1 = new Snake("Comba");
        snake1.eat();
        snake1.sleep();
        snake1.crawl();
        snake1.swim();
        System.out.println("-------------------------------------");
        Submarine submarine1 = new Submarine("SubmarineThailand", "Uncle Tu's submarine");
        submarine1.swim();
        System.out.println("-------------------------------------");
        Flyable[] flyableOdject = { bat1, bird1, plane1 };
        for (int i = 0; i < flyableOdject.length; i++) {
            flyableOdject[i].fly();
            flyableOdject[i].takeoff();
            flyableOdject[i].landing();
        }
        System.out.println("-------------------------------------");
        Crawlable[] crawlableOdject = { crocodile1, snake1 };
        for (int i = 0; i < crawlableOdject.length; i++) {
            crawlableOdject[i].crawl();
        }
        System.out.println("-------------------------------------");
        Swimable[] swimableOdject = {crocodile1,dog1,fish1,human1,submarine1};
        for (int i = 0; i < swimableOdject.length; i++) {
            swimableOdject[i].swim();
        }
        System.out.println("-------------------------------------");
        Walkable[] walkableObject = {bird1,cat1,dog1,human1,rat1};
        for (int i = 0; i < walkableObject.length; i++) {
            walkableObject[i].walk();
            walkableObject[i].run();
        }
        System.out.println("-------------------------------------");
        
    }
}
