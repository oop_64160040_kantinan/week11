package com.kantinanweek11;

public class Plane extends Vehicle implements Flyable{

    public Plane(String name, String enginName) {
        super(name, enginName);
    }

    @Override
    public void fly() {
        System.out.println(this.toString() + " fly.");
        
    }

    @Override
    public void takeoff() {
        System.out.println(this.toString() + " takeoff.");
        
    }

    @Override
    public void landing() {
        System.out.println(this.toString() + " landing.");
        
    }
    @Override
    public String toString() {
        return "Vehicle ("+this.getName()+") engine "+ this.getEnginName() ;
    }
}
