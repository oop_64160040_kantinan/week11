package com.kantinanweek11;

public class Cat extends Animal implements Walkable ,Landanimal{

    public Cat(String name) {
        super(name,4);
    }

    @Override
    public void walk() {
        System.out.println(this.toString() + "walk.");
        
    }

    @Override
    public void run() {
        System.out.println(this.toString() + "run.");
        
    }

    @Override
    public void eat() {
        System.out.println(this.toString() + "eat.");
        
    }

    @Override
    public void sleep() {
        System.out.println(this.toString() + "sleep.");
        
    }
    @Override
    public String toString() {
        return "Cat" + "(" + this.getName() + ")";
    }

}
