package com.kantinanweek11;

public abstract class Animal{
    private String name;
    private int numberOfLeg;
    public Animal(String name,int numberOfLeg){
        this.name = name;
        this.numberOfLeg = numberOfLeg;
    }
    public String getName(){
        return name;
    }
    public int getNumberOfLeg(){
        return numberOfLeg;
    }
    public String setName(String name){
        return this.name;
    }
    @Override
    public String toString(){
        return "Animal ("+name+") has "+ numberOfLeg +"leg";
    }
    public abstract void eat();
    public abstract void sleep();
}
