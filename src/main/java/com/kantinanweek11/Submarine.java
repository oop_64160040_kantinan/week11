package com.kantinanweek11;

public class Submarine extends Vehicle implements Swimable{

    public Submarine(String name, String enginName) {
        super(name, enginName);
    }

    @Override
    public void swim() {
        System.out.println(this.toString() + " swim.");
        
    }
    @Override
    public String toString() {
        return "Vehicle ("+this.getName()+") engine "+ this.getEnginName() ;
    }
    
}
