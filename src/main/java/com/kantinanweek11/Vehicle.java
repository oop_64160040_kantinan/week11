package com.kantinanweek11;

public abstract class Vehicle {
    private String name;
    private String enginName;
    public Vehicle(String name,String enginName){
        this.name = name;
        this.enginName = enginName;
    }
    public String getName(){
        return name;
    }
    public String getEnginName(){
        return enginName;
    }
    public void setName(){
        this.name = name;
    }
    public void setEnginName(){
        this.enginName = enginName;
    }
}
